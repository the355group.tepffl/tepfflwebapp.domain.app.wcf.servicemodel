﻿using TepfflWebApp.Domain.App.Web;
using TepfflWebApp.Domain.Core.Interfaces;
using TepfflWebApp.Domain.Infrastructure.Factories;

namespace TepfflWebApp.Domain.App.WCF.ServiceModel
{
    public class TfflServiceContract : ITfflServiceContract
    {
        public TfflServiceContract()
            : base()
        {
            
        }   
        
        public ITfflWebSessionService CreateWebSession()
        {
            return WebSessionServiceFactory.Create(new SystemWebHttpContextAccessor(System.Web.HttpContext.Current));
        }
    }
}
